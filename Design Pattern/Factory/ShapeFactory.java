package Factory;

public class ShapeFactory {
	private Shape shape;
	public Shape getShape(String shapeF) {
		if(shapeF.equals("CIRCLE")) {
			shape = new Circle();
		}else if(shapeF.equals("RECTANGLE") ) {
			shape = new Rectangle();
		}else if(shapeF.equals("SQUARE") ) {
			shape = new Square();
		}
		return shape ;
	}

}
