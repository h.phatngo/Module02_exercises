package Proxy;

public class main {
	public static void main(String[] args) {
		Image image = new ProxyImage("git.png");
		
		//Image will be loaded from disk
		image.display();
		System.out.println("");
		
		//Image will not be loaded from disk
		image.display();
	}
}
