package SoSanh_Strategy;

public class main {
	public static void main(String[] args) {
		Person person01 = new Person("Ngo Phat1", 20, 165);
		Person person02 = new Person("Ngo Nhan", 22, 138);
		
		System.out.println("So Sánh thằng A với thằng B");
		System.out.print("Thằng A:  ");
		System.out.println(person01.getName() +"  "+person01.getTuoi()+"  "+person01.getHouseNum()+"\n");
		
		System.out.print("Thằng B: ");
		System.out.println(person02.getName() +"  "+person02.getTuoi()+"  "+ person02.getHouseNum());
		System.out.println("-----------------------------------------------------");
		
		CachSoSanh theoTen = Ultis.SoSanhTheoTen();
		int iTheoTen = theoTen.SoSanh(person01, person02);
		System.out.println("Tên thế nào ? : " + iTheoTen);
		
		CachSoSanh theoTuoi = Ultis.SoSanhTheoTuoi();
		int iTheoTuoi = theoTuoi.SoSanh(person01, person02);
		System.out.println("Thế còn Tuổi ? : " + iTheoTuoi);
		
		CachSoSanh theoHouseNum = Ultis.SoSanhTheoSoNha();
		int iTheoSoNha = theoHouseNum.SoSanh(person01, person02);
		System.out.println("Số nhà thì sao ? : " + iTheoSoNha);
	}
}
