package SoSanh_Strategy;

public class SoSanhTheoSoNha implements CachSoSanh {

	@Override
	public int SoSanh(Person a, Person b) {
		int i=0;
		long aHouse = a.getHouseNum();
		long bHouse = b.getHouseNum();
		if(aHouse > bHouse) {
			i=1;
		}else if(aHouse == bHouse) {
			i=0;
		}else {
			i=-1;
		}
		return i;
	}

}
