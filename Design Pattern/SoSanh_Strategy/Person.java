package SoSanh_Strategy;

public class Person {
	private String name;
	private int age;
	private long houseNum;

	public String getName() {
		return name;
	}

	public int getTuoi() {
		return age;
	}

	public long getHouseNum() {
		return houseNum;
	}

	public Person (String name, int tuoi, long houseNum) {
		super();
		this.name=name;
		this.age=tuoi;
		this.houseNum=houseNum;
	}
}	
