package SoSanh_Strategy;

public class SoSanhTheoTuoi implements CachSoSanh{

	@Override
	public int SoSanh(Person a, Person b) {
		int i=0;
		int aAge = a.getTuoi();
		int bAge = b.getTuoi();
		if(aAge > bAge) {
			i=1;
		}else if(aAge == bAge) {
			i=0;
		}else {
			i=-1;
		}
		return i;
	}
	
}
