package SoSanh_Strategy;

public class SoSanhTheoTen implements CachSoSanh{

	@Override
	public int SoSanh(Person a, Person b) {
		int i=0;
		int aName = a.getName().length();
		int bName = b.getName().length();
		if(aName > bName) {
			i=1;
		}else if(aName == bName) {
			i=0;
		}else {
			i=-1;
		}
		return i;
	}
}
