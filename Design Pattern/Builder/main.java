package Builder;

public class main {
	public static void main(String[] args) {
		Person person = Person.getBuilder("Ngo Phat", "Gia kiem")
				.age(12).email("ngosongtu166@gmail.com").age(13).build();
		
		System.out.println(person.toString());
	}
}
