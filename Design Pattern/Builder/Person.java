package Builder;

public class Person {
	private  String name;
	private  String addr;
	private  int age;
	private String email;
	
	public static sBuilder getBuilder(String name, String addr) {
		return new sBuilder(name, addr);
	}
	
	private Person(sBuilder sbuilder) {
		this.name = sbuilder.name;
		this.addr = sbuilder.addr;
		this.age = sbuilder.age;
		this.email = sbuilder.email;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", addr=" + addr + ", age=" + age + ", email=" + email + "]";
	}

	public static class sBuilder{
		private String name;
		private String addr;
		private int age;
		private String email;
		private sBuilder(String name, String addr) {
			this.name = name;
			this.addr = addr;
		}
		
		public sBuilder age(int age) {
			this.age = age;
			return this;
		}
		public sBuilder email(String email) {
			this.email = email;
			return this;
		}
		
		public Person build() {
			return new Person(this);
		}
	}
}
