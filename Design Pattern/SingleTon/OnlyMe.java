package SingleTon;

public class OnlyMe {
	private static OnlyMe me = new OnlyMe();
	private OnlyMe() {
		
	}
	public static OnlyMe getMe() {
		return me ;
	}

}
