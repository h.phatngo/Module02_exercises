package Buoi04;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Exercise4_1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtFullName;
	private JTextField txtImagePath;
	private JTextField txtPhoneNumber;
	private Document doc;
	private Element root;
	private File file = new File("\\Projects\\Module02_exercises\\src\\Buoi04\\themlienhe.xml");
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercise4_1 frame = new Exercise4_1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public Exercise4_1() throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(file);
		
		root = doc.getDocumentElement();
		
		
		setTitle("Th\u00EAm m\u1EDBi li\u00EAn h\u1EC7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblThmMiLin = new JLabel("Th\u00EAm m\u1EDBi li\u00EAn h\u1EC7");
		lblThmMiLin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblThmMiLin.setBounds(31, 30, 127, 21);
		contentPane.add(lblThmMiLin);
		
		JLabel lblHTn = new JLabel("H\u1ECD T\u00EAn:");
		lblHTn.setBounds(31, 75, 46, 14);
		contentPane.add(lblHTn);
		
		JLabel lbltd = new JLabel("\u0110TD\u0110:");
		lbltd.setBounds(31, 116, 46, 14);
		contentPane.add(lbltd);
		
		JLabel lblHnhnh = new JLabel("H\u00ECnh \u1EA2nh:");
		lblHnhnh.setBounds(32, 154, 68, 14);
		contentPane.add(lblHnhnh);
		
		txtFullName = new JTextField();
		txtFullName.setBounds(87, 72, 186, 21);
		contentPane.add(txtFullName);
		txtFullName.setColumns(10);
		
		txtImagePath = new JTextField();
		txtImagePath.setColumns(10);
		txtImagePath.setBounds(87, 151, 186, 21);
		contentPane.add(txtImagePath);
		
		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setColumns(10);
		txtPhoneNumber.setBounds(87, 113, 127, 21);
		contentPane.add(txtPhoneNumber);
		
		JLabel lbImage = new JLabel("");
		lbImage.setBounds(345, 60, 159, 161);
		contentPane.add(lbImage);
		
		JButton btnImagePath = new JButton("...");
		btnImagePath.addActionListener((e) -> {
			
		});
		btnImagePath.setBounds(286, 145, 40, 32);
		contentPane.add(btnImagePath);
		
		JButton btnAdd = new JButton("Th\u00EAm m\u1EDBi");
		btnAdd.addActionListener((e) -> {
			
			Element lienhe = doc.createElement("lienhe");
			
			Element hoten = doc.createElement("hoten");
			hoten.setTextContent(txtFullName.getText());
			lienhe.appendChild(hoten);
			
			Element sdt = doc.createElement("SDT");
			sdt.setTextContent(txtPhoneNumber.getText());
			lienhe.appendChild(sdt);
			
			Element imagePath = doc.createElement("imagePath");
			imagePath.setTextContent(txtImagePath.getText());
			lienhe.appendChild(imagePath);
			
			root.appendChild(lienhe);
			
			TransformerFactory transFactory = TransformerFactory.newInstance();
			try {
				Transformer transformer = transFactory.newTransformer();
				
				DOMSource xmlSource = new DOMSource(doc);
				StreamResult outputTarget = new StreamResult(file);
				transformer.transform(xmlSource, outputTarget);
			} catch (TransformerException e1) {
				e1.printStackTrace();
			}
		});
		btnAdd.setBounds(87, 201, 95, 23);
		contentPane.add(btnAdd);
	}
}
