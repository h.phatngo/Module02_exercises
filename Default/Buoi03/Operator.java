package Buoi03;

public class Operator {
	private double a;
	private double b;
	public Operator(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double Cong() {
		FuntionalInterfaceMath cong = (a,b) -> a+b;
		return cong.Operator(a, b);
	}
	
	public double Tru() {
		FuntionalInterfaceMath tru = (a,b) -> a-b;
		return tru.Operator(a, b);
	}
	
	public double Nhan() {
		FuntionalInterfaceMath nhan = (a,b) -> a*b;
		return nhan.Operator(a, b);
	}
	
	public double Chia() {
		FuntionalInterfaceMath chia = (a,b) -> a/b;
		return chia.Operator(a, b);
	}
}
