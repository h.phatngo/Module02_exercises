package Buoi03;

public class CalArea {
	private double x1;
	private double x2;
	

	public CalArea(double x1, double x2) {
		super();
		this.x1 = x1;
		this.x2 = x2;
	}

	public double Circle() {
		FuntionalInterfaceArea area = (pi,r) -> pi*r*r;
		return area.calArea(x1, x2); 
	}
	
	public double Rectangle() {
		FuntionalInterfaceArea area = (a,b) -> a*b;
		return area.calArea(x1, x2); 
	}
}
