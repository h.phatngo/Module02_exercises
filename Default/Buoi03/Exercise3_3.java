package Buoi03;

import java.util.Scanner;

import javax.print.DocFlavor.INPUT_STREAM;

public class Exercise3_3 {
	public static void main(String[] args) {
		//1.Khai bao doi tuong scanner
		Scanner scanner = new Scanner(System.in);
		
		//2.Dien tich hinh tron
		System.out.print("Nhap ban kinh: ");
		double r = scanner.nextDouble();
		CalArea circle = new CalArea(Math.PI, r);
		System.out.println("Dien tich hinh tron la: "+ circle.Circle());
		
		//3.Dien tich hinh chu nhat
		System.out.print("Nhap a: ");
		double a = scanner.nextDouble();
		System.out.print("Nhap b: ");
		double b = scanner.nextDouble();
		CalArea rectangle = new CalArea(a, b);
		System.out.println("Dien tich hinh chu nhat la: "+rectangle.Rectangle());
	}
}
