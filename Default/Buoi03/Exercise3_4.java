package Buoi03;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class Exercise3_4 {
	public static void main(String[] args) {
		//1.Khai bao doi tuong
		Scanner scanner = new Scanner(System.in);
		System.out.println("Nhap a va b: ");
		System.out.print("a= ");
		double a = scanner.nextDouble();
		System.out.print("b= ");
		double b = scanner.nextDouble();
		Operator operator = new Operator(a, b);
		
		//2.Cong, Tru, Nhan, Chia
		System.out.println("Tong cua a va b la: "+ operator.Cong());
		System.out.println("Hieu cua a va b la: "+ operator.Tru());
		System.out.println("Tich cua a va b la: "+ operator.Nhan());
		System.out.println("Thuong cua a va b la: "+ operator.Chia());
	}
}
