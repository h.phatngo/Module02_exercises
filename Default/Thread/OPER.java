package Thread;

public enum OPER {
	TONG,
	HIEU,
	NHAN,
	CHIA;
	
	float tinh(int a, int b) {
		float result=0;
		switch (this) {
		case TONG: 
			result = a+b;
			break;

		case HIEU: 
			result = a -b;
			break;
		case NHAN: 
			result = a*b;
			break;
		case CHIA:
			result = (float) ((1.0*a)/b);
			break;
		}
		return result;
	}
}
