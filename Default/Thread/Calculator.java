package Thread;

public class Calculator extends Thread{
	private User user;
	private int numbA;
	private int numbB;
	private OPER oper;
	
	public Calculator(User user) {
		this.user=user;
	}
	
	@Override
	public void run() {
		System.out.println(String.format("USER %s la %.2f :", user.getName(),
				oper.tinh(numbA, numbB)));
	}
	
	public void tinhTong(int a, int b) {
		numbA = a;
		numbB = b;
		oper = OPER.TONG;
		this.start();
	}
	public void tinhHieu(int a, int b) {
		numbA = a;
		numbB = b;
		oper = OPER.HIEU;
		this.start();
	}
	
	public void tinhNhan(int a, int b) {
		numbA = a;
		numbB = b;
		oper = OPER.NHAN;
		this.start();
	}
	public void tinhThuong(int a, int b) {
		numbA = a;
		numbB = b;
		oper = OPER.CHIA;
		this.start();
	}
}
