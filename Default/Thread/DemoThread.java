package Thread;

public class DemoThread {
	public static void main(String[] args) {
		Thread soLe = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 1; i <= 100; i = i + 1) {
					System.out.println("Thread01: " + i);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		Thread soChan = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i <= 100; i = i + 2) {
					System.out.println("Thread02: " + i);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		soLe.start();
		soChan.start();
	}
}
