package Buoi01;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JSplitPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.awt.event.ActionEvent;

public class Exercise01_2 extends JFrame {

	private JPanel contentPane;
	private JTextField txtWordInsert;
	private JTextField txtMeaningInsert;
	private JTextField txtWordSearch;
	private JTextField txtMeaningSearch;
	private DefaultListModel listModel;
	private Map<String,String> hashMap;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercise01_2 frame = new Exercise01_2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Exercise01_2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 528, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 512, 262);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Show - Insert word", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblWord = new JLabel("Word: ");
		lblWord.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblWord.setBounds(22, 24, 48, 21);
		panel_1.add(lblWord);
		
		txtWordInsert = new JTextField();
		txtWordInsert.setBounds(67, 25, 100, 20);
		panel_1.add(txtWordInsert);
		txtWordInsert.setColumns(10);
		
		JLabel lblWord_1 = new JLabel("Meaning:");
		lblWord_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblWord_1.setBounds(212, 24, 59, 21);
		panel_1.add(lblWord_1);
		
		txtMeaningInsert = new JTextField();
		txtMeaningInsert.setColumns(10);
		txtMeaningInsert.setBounds(281, 25, 110, 20);
		panel_1.add(txtMeaningInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 64, 395, 159);
		panel_1.add(scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		
		hashMap = new HashMap<>();
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listModel = new DefaultListModel<>();
				
				hashMap.put(txtWordInsert.getText(), txtMeaningInsert.getText());
				
				for (Map.Entry entry : hashMap.entrySet()) {
					listModel.addElement(entry.toString());
				}
				list.setModel(listModel);
			}
		});
		btnInsert.setBounds(408, 24, 89, 23);
		panel_1.add(btnInsert);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Search", null, panel, null);
		panel.setLayout(null);
		
		JLabel label = new JLabel("Word: ");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label.setBounds(23, 24, 48, 21);
		panel.add(label);
		
		txtWordSearch = new JTextField();
		txtWordSearch.setColumns(10);
		txtWordSearch.setBounds(92, 25, 100, 20);
		panel.add(txtWordSearch);
		
		JLabel label_1 = new JLabel("Meaning:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(23, 67, 59, 21);
		panel.add(label_1);
		
		txtMeaningSearch = new JTextField();
		txtMeaningSearch.setColumns(10);
		txtMeaningSearch.setBounds(92, 68, 110, 20);
		panel.add(txtMeaningSearch);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sWord = txtWordSearch.getText();
				for(Map.Entry entry : hashMap.entrySet()) {
					
					if(sWord.equalsIgnoreCase(entry.getKey().toString())) {
						txtMeaningSearch.setText(entry.getValue().toString());
					}else if(!sWord.equalsIgnoreCase(entry.getKey().toString())){
						JOptionPane.showMessageDialog(null, "Kh�ng c�");
					}
				}
			}
		});
		btnSearch.setBounds(232, 24, 89, 23);
		panel.add(btnSearch);
	}
}
