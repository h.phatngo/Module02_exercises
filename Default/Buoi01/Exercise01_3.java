package Buoi01;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.awt.event.ActionEvent;
import javax.swing.JList;

public class Exercise01_3 extends JFrame {

	private JPanel contentPane;
	private JTextField txtKey;
	private JTextField txtValue;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercise01_3 frame = new Exercise01_3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Exercise01_3() {
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKey = new JLabel("Key:");
		lblKey.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblKey.setBounds(26, 37, 44, 24);
		contentPane.add(lblKey);
		
		JLabel lblValue = new JLabel("Value:");
		lblValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblValue.setBounds(217, 37, 44, 24);
		contentPane.add(lblValue);
		
		txtKey = new JTextField();
		txtKey.setBounds(69, 37, 106, 24);
		contentPane.add(txtKey);
		txtKey.setColumns(10);
		
		txtValue = new JTextField();
		txtValue.setColumns(10);
		txtValue.setBounds(271, 38, 106, 24);
		contentPane.add(txtValue);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 140, 379, 111);
		contentPane.add(scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		DefaultListModel listModel = new DefaultListModel<>();
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sKey 		= txtKey.getText();
				String sValue 		= txtValue.getText();
				
				Map mapStudents = new TreeMap<>();
				mapStudents.put(sKey, sValue);				
				listModel.addElement(mapStudents.toString());
				list.setModel(listModel);
			}
		});
		btnAdd.setBounds(70, 85, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnContinue = new JButton("Continue");
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtKey.setText(null);
				txtValue.setText(null);
			}
		});
		btnContinue.setBounds(199, 85, 89, 23);
		contentPane.add(btnContinue);
		
	

	}
}
