package Buoi01;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.event.ActionEvent;

public class Exercise01_1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercise01_1 frame = new Exercise01_1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Exercise01_1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 427);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoiMng = new JLabel("Lo\u1EA1i m\u1EA3ng:");
		lblLoiMng.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblLoiMng.setBounds(44, 37, 76, 21);
		contentPane.add(lblLoiMng);
		
		JComboBox jCbBox = new JComboBox();
		jCbBox.setModel(new DefaultComboBoxModel(new String[] {"Chu\u1ED7i", "S\u1ED1 Nguy\u00EAn", "S\u1ED1 Th\u1EF1c"}));
		jCbBox.setBounds(177, 38, 135, 21);
		contentPane.add(jCbBox);
		
		JLabel lblNhpMngmi = new JLabel("Nh\u1EADp m\u1EA3ng (M\u1ED7i ph\u1EA7n t\u1EED c\u00E1ch nhau b\u1EB1ng 1 kho\u1EA3ng tr\u1EAFng):");
		lblNhpMngmi.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNhpMngmi.setBounds(44, 97, 357, 27);
		contentPane.add(lblNhpMngmi);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 146, 343, 55);
		contentPane.add(scrollPane);
		
		JTextArea txtNhapMang = new JTextArea();
		scrollPane.setViewportView(txtNhapMang);
		
		JLabel lblMngSauKhi = new JLabel("M\u1EA3ng sau khi s\u1EAFp x\u1EBFp:");
		lblMngSauKhi.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMngSauKhi.setBounds(44, 230, 357, 27);
		contentPane.add(lblMngSauKhi);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(44, 279, 341, 53);
		contentPane.add(scrollPane_1);
		
		JTextArea txtSapXepMang = new JTextArea();
		scrollPane_1.setViewportView(txtSapXepMang);
		
		JButton btnSapXepMang = new JButton("S\u1EAFp x\u1EBFp m\u1EA3ng");
		btnSapXepMang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int iComboBox 		= jCbBox.getSelectedIndex();
				String[] sMangInput		= txtNhapMang.getText().split(" ");
				List listMang 	= new ArrayList<>();
				
				if(iComboBox == 0) {
					for(int i =0; i<sMangInput.length ; i++) {
						listMang.add(sMangInput[i]);
					}
				}else if(iComboBox == 1) {
					for(int i =0; i<sMangInput.length ; i++) {
						listMang.add(Integer.parseInt(sMangInput[i]));
					}
				}else {
					for(int i =0; i<sMangInput.length ; i++) {
						listMang.add(Double.parseDouble(sMangInput[i]));
					}
				}
				Collections.sort(listMang);
				txtSapXepMang.setText(listMang.toString());
			}
		});
		btnSapXepMang.setBounds(162, 355, 126, 23);
		contentPane.add(btnSapXepMang);
	}
}
